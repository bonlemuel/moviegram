import { actionTypes } from 'actions/AuthActions';

const initialState = {
  token: null,
  session_id: null,
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.TOKEN_SUCCESS:
      return {
        ...state,
        token: action.token.request_token,
      };
    case actionTypes.CREATE_SESSION_REQUEST_SUCCESS:
      return {
        ...state,
        session_id: action.session.session_id,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};

export default authReducer;
