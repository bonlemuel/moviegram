import { combineReducers } from 'redux';
import error from './ErrorReducer';
import user from './UserReducer';
import token from './AuthReducer';
import movie from './MovieReducer';
import status from './StatusReducer';

import storage from 'redux-persist/lib/storage';

import { Alert } from 'react-native';

const appReducer = combineReducers({
  error,
  user,
  token,
  movie,
  status,
})

const rootReducer = (state, action) => {
  console.log("[ REDUX ACTION ] ", action.type);
  console.log("[ REDUX STATE ] ", state)
  // native alert
  if(action.type === "ADD_MOVIE_WATCHLIST_REQUEST_SUCCESS"){  
    Alert.alert(
      'Movie Added',
      'Movie added to your watchlist.',
      [
        {
          text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }

  if(action.type === "RATE_MOVIE_REQUEST_SUCCESS"){  
    Alert.alert(
      'Rating Successful',
      'Movie rating has been submitted. Thank you!',
      [
        {
          text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }

  if(action.type === "UNRATE_MOVIE_REQUEST_SUCCESS"){  
    Alert.alert(
      'Rating Deleted',
      'Your movie rating has been successfully deleted!',
      [
        {
          text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }

  if(action.type === "REMOVE_MOVIE_WATCHLIST_REQUEST_SUCCESS"){  
    Alert.alert(
      'Remove Watchlist',
      'Movie has been removed from your watchlist!',
      [
        {
          text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }
  
  if (action.type === 'LOGOUT') {
    storage.removeItem('persist:root')
    state = undefined
    console.log("********** All Redux Reducer has been reset **********")
  }
  return appReducer(state, action)
}

export default rootReducer;
