import { actionTypes } from 'actions/MovieActions';

const initialState = {
  today: null,
  weekly: null,
  rated: null,
  selected: null,
  search: null,
  watchlist: null,
};

const movieReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_TODAY_TRENDING_SUCCESS:
      return {
        ...state,
        today: action.movies,
      };
    case actionTypes.GET_WEEKLY_TRENDING_SUCCESS:
      return {
        ...state,
        weekly: action.movies,
      };
    case actionTypes.GET_MOVIE_DETAILS_SUCCESS:
      return {
        ...state,
        selected: action.movies,
      };
    case actionTypes.SEARCH_MOVIE_REQUEST_SUCCESS:
      return {
        ...state,
        search: action.movies,
      };
    case actionTypes.SEARCH_MOVIE_REQUEST_RESET:
      return {
        ...state,
        search: null
      };
    case actionTypes.GET_WATCHLIST_REQUEST_SUCCESS:
      return {
        ...state,
        watchlist: action.movies
      };
    case actionTypes.GET_RATED_MOVIE_REQUEST_SUCCESS:
    return {
      ...state,
      rated: action.movies
    };
    case actionTypes.CLEAR_SELECTED:
      return {
        ...state,
        selected: null,
      };
      default:
        return state;
  }
};

export default movieReducer;
