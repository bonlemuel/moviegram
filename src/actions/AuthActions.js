import AuthController from '../controllers/AuthController';

export const actionTypes = {
  TOKEN_REQUEST: 'TOKEN_REQUEST',
  TOKEN_SUCCESS: 'TOKEN_SUCCESS',
  TOKEN_ERROR: 'TOKEN_ERROR',
  CREATE_SESSION_REQUEST: 'CREATE_SESSION_REQUEST',
  CREATE_SESSION_REQUEST_ERROR: 'CREATE_SESSION_REQUEST_ERROR',
  CREATE_SESSION_REQUEST_SUCCESS: 'CREATE_SESSION_REQUEST_SUCCESS',
  LOGOUT: 'LOGOUT',
};

const tokenRequest = () => ({
  type: actionTypes.TOKEN_REQUEST,
});

const tokenSuccess = token => ({
  type: actionTypes.TOKEN_SUCCESS,
  token,
});

const tokenError = error => ({
  type: actionTypes.TOKEN_ERROR,
  error,
});

const createSessionRequest = () => ({
  type: actionTypes.CREATE_SESSION_REQUEST,
});

const createSessionError = error => ({
  type: actionTypes.CREATE_SESSION_REQUEST_ERROR,
  error,
});

const createSessionSuccess = session => ({
  type: actionTypes.CREATE_SESSION_REQUEST_SUCCESS,
  session,
});

const logoutRequest = () => ({
  type: actionTypes.LOGOUT,
});

export const initToken = () => async (dispatch) => {
  dispatch(tokenRequest());
  try {
    const token = await AuthController.token();
    dispatch(tokenSuccess(token));
  } catch (error) {
    dispatch(tokenError(error.message));
  }
};

export const requestSession = (token) => async (dispatch) => {
  dispatch(createSessionRequest());
  try {
    const session = await AuthController.createSession(token);
    dispatch(createSessionSuccess(session));
  } catch (error) {
    dispatch(createSessionError(error.message));
  }
};

export const logout = () => (dispatch) => {
  AuthController.logout();
  dispatch(logoutRequest());
};
