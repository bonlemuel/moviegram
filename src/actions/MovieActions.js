import MovieController from '../controllers/MovieController';

export const actionTypes = {
  GET_TODAY_TRENDING: 'GET_TODAY_TRENDING',
  GET_TODAY_TRENDING_SUCCESS: 'GET_TODAY_TRENDING_SUCCESS',
  GET_TODAY_TRENDING_ERROR: 'GET_TODAY_TRENDING_ERROR',
  GET_WEEKLY_TRENDING: 'GET_WEEKLY_TRENDING',
  GET_WEEKLY_TRENDING_SUCCESS: 'GET_WEEKLY_TRENDING_SUCCESS',
  GET_WEEKLY_TRENDING_ERROR: 'GET_WEEKLY_TRENDING_ERROR',
  GET_MOVIE_DETAILS: 'GET_MOVIE_DETAILS',
  GET_MOVIE_DETAILS_SUCCESS: 'GET_MOVIE_DETAILS_SUCCESS',
  GET_MOVIE_DETAILS_ERROR: 'GET_MOVIE_DETAILS_ERROR',
  GET_WATCHLIST_REQUEST: 'GET_WATCHLIST_REQUEST',
  GET_WATCHLIST_REQUEST_SUCCESS: 'GET_WATCHLIST_REQUEST_SUCCESS',
  GET_WATCHLIST_REQUEST_ERROR: 'GET_WATCHLIST_REQUEST_ERROR',
  GET_RATED_MOVIE_REQUEST: 'GET_RATED_MOVIE_REQUEST',
  GET_RATED_MOVIE_REQUEST_SUCCESS: 'GET_RATED_MOVIE_REQUEST_SUCCESS',
  GET_RATED_MOVIE_REQUEST_ERROR: 'GET_RATED_MOVIE_REQUEST_ERROR',
  SEARCH_MOVIE_REQUEST: 'SEARCH_MOVIE_REQUEST',
  SEARCH_MOVIE_REQUEST_SUCCESS: 'SEARCH_MOVIE_REQUEST_SUCCESS',
  SEARCH_MOVIE_REQUEST_RESET: 'SEARCH_MOVIE_REQUEST_RESET',
  SEARCH_MOVIE_REQUEST_ERROR: 'SEARCH_MOVIE_REQUEST_ERROR',
  ADD_MOVIE_WATCHLIST_REQUEST: 'ADD_MOVIE_WATCHLIST_REQUEST',
  ADD_MOVIE_WATCHLIST_REQUEST_SUCCESS: 'ADD_MOVIE_WATCHLIST_REQUEST_SUCCESS',
  ADD_MOVIE_WATCHLIST_REQUEST_ERROR: 'ADD_MOVIE_WATCHLIST_REQUEST_ERROR',
  REMOVE_MOVIE_WATCHLIST_REQUEST: 'REMOVE_MOVIE_WATCHLIST_REQUEST',
  REMOVE_MOVIE_WATCHLIST_REQUEST_SUCCESS: 'REMOVE_MOVIE_WATCHLIST_REQUEST_SUCCESS',
  REMOVE_MOVIE_WATCHLIST_REQUEST_ERROR: 'REMOVE_MOVIE_WATCHLIST_REQUEST_ERROR',
  RATE_MOVIE_REQUEST: 'RATE_MOVIE_REQUEST',
  RATE_MOVIE_REQUEST_SUCCESS: 'RATE_MOVIE_REQUEST_SUCCESS',
  RATE_MOVIE_REQUEST_ERROR: 'RATE_MOVIE_REQUEST_ERROR',
  UNRATE_MOVIE_REQUEST: 'UNRATE_MOVIE_REQUEST',
  UNRATE_MOVIE_REQUEST_SUCCESS: 'UNRATE_MOVIE_REQUEST_SUCCESS',
  UNRATE_MOVIE_REQUEST_ERROR: 'UNRATE_MOVIE_REQUEST_ERROR',
  CLEAR_SELECTED: 'CLEAR_SELECTED',
  LOGOUT: 'LOGOUT',
};

const getTodayTrending = () => ({
  type: actionTypes.GET_TODAY_TRENDING,
});

const getTodayTrendingSuccess = movies => ({
  type: actionTypes.GET_TODAY_TRENDING_SUCCESS,
  movies,
});

const getTodayTrendingError = error => ({
  type: actionTypes.GET_TODAY_TRENDING_ERROR,
  error,
});

const getWeeklyTrending = () => ({
  type: actionTypes.GET_WEEKLY_TRENDING,
});

const getWeeklyTrendingSuccess = movies => ({
  type: actionTypes.GET_WEEKLY_TRENDING_SUCCESS,
  movies,
});

const getWeeklyTrendingError = error => ({
  type: actionTypes.GET_WEEKLY_TRENDING_ERROR,
  error,
});

const getMovieDetailsRequest = () => ({
  type: actionTypes.GET_MOVIE_DETAILS,
});

const getMovieDetailsSuccess = movies => ({
  type: actionTypes.GET_MOVIE_DETAILS_SUCCESS,
  movies,
});

const getMovieDetailsError = error => ({
  type: actionTypes.GET_MOVIE_DETAILS_ERROR,
  error,
});

const getSearchMovieRequest = () => ({
  type: actionTypes.SEARCH_MOVIE_REQUEST,
});

const getSearchMovieRequestSuccess = movies => ({
  type: actionTypes.SEARCH_MOVIE_REQUEST_SUCCESS,
  movies,
});

const getSearchMovieRequestReset = () => ({
  type: actionTypes.SEARCH_MOVIE_REQUEST_RESET,
});

const getSearchMovieRequestError = error => ({
  type: actionTypes.SEARCH_MOVIE_REQUEST_ERROR,
  error,
});

const getWatchlistRequest = () => ({
  type: actionTypes.GET_WATCHLIST_REQUEST,
});

const getWatchlistRequestSuccess = movies => ({
  type: actionTypes.GET_WATCHLIST_REQUEST_SUCCESS,
  movies,
});

const getWatchlistRequestError = error => ({
  type: actionTypes.GET_MOVIE_DETAILS_ERROR,
  error,
});

const getRatedMovieRequest = () => ({
  type: actionTypes.GET_RATED_MOVIE_REQUEST,
});

const getRatedMovieRequestSuccess = movies => ({
  type: actionTypes.GET_RATED_MOVIE_REQUEST_SUCCESS,
  movies,
});

const getRatedMovieRequestError = error => ({
  type: actionTypes.GET_RATED_MOVIE_REQUEST_ERROR,
  error,
});

const addMovieWatchListRequest = () => ({
  type: actionTypes.ADD_MOVIE_WATCHLIST_REQUEST,
});

const addMovieWatchListRequestSuccess = movies => ({
  type: actionTypes.ADD_MOVIE_WATCHLIST_REQUEST_SUCCESS,
  movies,
});

const addMovieWatchListRequestError = error => ({
  type: actionTypes.ADD_MOVIE_WATCHLIST_REQUEST_ERROR,
  error,
});

const removeMovieWatchListRequest = () => ({
  type: actionTypes.REMOVE_MOVIE_WATCHLIST_REQUEST,
});

const removeMovieWatchListRequestSuccess = movies => ({
  type: actionTypes.REMOVE_MOVIE_WATCHLIST_REQUEST_SUCCESS,
  movies,
});

const removeMovieWatchListRequestError = error => ({
  type: actionTypes.REMOVE_MOVIE_WATCHLIST_REQUEST_ERROR,
  error,
});

const rateMovieRequest = () => ({
  type: actionTypes.RATE_MOVIE_REQUEST,
});

const rateMovieRequestSuccess = movies => ({
  type: actionTypes.RATE_MOVIE_REQUEST_SUCCESS,
  movies,
});

const rateMovieRequestError = error => ({
  type: actionTypes.RATE_MOVIE_REQUEST_ERROR,
  error,
});

const unrateMovieRequest = () => ({
  type: actionTypes.UNRATE_MOVIE_REQUEST,
});

const unrateMovieRequestSuccess = movies => ({
  type: actionTypes.UNRATE_MOVIE_REQUEST_SUCCESS,
  movies,
});

const unrateMovieRequestError = error => ({
  type: actionTypes.UNRATE_MOVIE_REQUEST_ERROR,
  error,
});

const clearSelectedMovie = () => ({
  type: actionTypes.CLEAR_SELECTED,
});

const logoutRequest = () => ({
  type: actionTypes.LOGOUT,
});

export const getTodayTrendingMovies = () => async (dispatch) => {
  dispatch(getTodayTrending())
  try {
    const movie = await MovieController.getTrendingMovie('day');
    dispatch(getTodayTrendingSuccess(movie));
  } catch (error) {
    dispatch(getTodayTrendingError(error.message));
  }
};

export const getWeeklyTrendingMovies = () => async (dispatch) => {
  dispatch(getWeeklyTrending())
  try {
    const movie = await MovieController.getTrendingMovie('week');
    dispatch(getWeeklyTrendingSuccess(movie));
  } catch (error) {
    dispatch(getWeeklyTrendingError(error.message));
  }
};

export const getMovieDetailsDispatch = (id) => async (dispatch) => {
  dispatch(getMovieDetailsRequest())
  try {
    const movie = await MovieController.getMovieDetails(id);
    dispatch(getMovieDetailsSuccess(movie));
  } catch (error) {
    dispatch(getMovieDetailsError(error.message));
  }
};

export const getSearchMovieDispatch = (query) => async (dispatch) => {
  dispatch(getSearchMovieRequest())
  try {
    if(query !== ''){
      const movie = await MovieController.getSearchMovie(query);
      dispatch(getSearchMovieRequestSuccess(movie));
    } else {
      dispatch(getSearchMovieRequestReset());
    }
  } catch (error) {
    dispatch(getSearchMovieRequestError(error.message));
  }
};

export const getWatchListDispatch = (sessionid) => async (dispatch) => {
  dispatch(getWatchlistRequest())
  try {
    const movie = await MovieController.getWatchlist(sessionid);
    dispatch(getWatchlistRequestSuccess(movie));
  } catch (error) {
    dispatch(getWatchlistRequestError(error.message));
  }
};

export const addMovieWatchListDispatch = (movieid, sessionid) => async (dispatch) => {
  dispatch(addMovieWatchListRequest())
  try {
    const movie = await MovieController.addToWatchlist(movieid, sessionid);
    dispatch(addMovieWatchListRequestSuccess(movie));
  } catch (error) {
    dispatch(addMovieWatchListRequestError(error.message));
  }
};

export const removeMovieWatchListDispatch = (movieid, sessionid) => async (dispatch) => {
  dispatch(removeMovieWatchListRequest())
  try {
    const movie = await MovieController.removeFromWatchlist(movieid, sessionid);
    dispatch(removeMovieWatchListRequestSuccess(movie));
  } catch (error) {
    dispatch(removeMovieWatchListRequestError(error.message));
  }
};

export const getRatedMovieDispatch = (sessionid) => async (dispatch) => {
  dispatch(getRatedMovieRequest())
  try {
    const movie = await MovieController.getRatedMovies(sessionid);
    dispatch(getRatedMovieRequestSuccess(movie));
  } catch (error) {
    dispatch(getRatedMovieRequestError(error.message));
  }
};

export const rateMovieDispatch = (rating, movieid, sessionid) => async (dispatch) => {
  dispatch(rateMovieRequest())
  try {
    const movie = await MovieController.rateMovie(rating, movieid, sessionid);
    dispatch(rateMovieRequestSuccess(movie));
  } catch (error) {
    dispatch(rateMovieRequestError(error.message));
  }
};

export const unrateMovieDispatch = (movieid, sessionid) => async (dispatch) => {
  dispatch(unrateMovieRequest())
  try {
    const movie = await MovieController.unrateMovie(movieid, sessionid);
    dispatch(unrateMovieRequestSuccess(movie));
  } catch (error) {
    dispatch(unrateMovieRequestError(error.message));
  }
};

export const clearSelected = () => async (dispatch) => {
  dispatch(clearSelectedMovie())
};


