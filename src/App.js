import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator, StyleSheet, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import Navigation from './components/navigation';
import Colors from './helpers/Colors';
import { store, persist } from './reducers';

console.disableYellowBox = true;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
});

export default function App() {
  const [ready, setReady] = useState(false);

  useEffect(() => {
    persist(() => {
      setReady(true);
    });
  });

  const statusBar = (
    <StatusBar
      barStyle="light-content"
      backgroundColor={Colors.background}
    />
  )

  const loading = (
    <View style={styles.container}>
      {statusBar}
      <ActivityIndicator />
    </View>
  );

  const loaded = (
    <Provider store={store}>
       {statusBar}
      <Navigation />
    </Provider>
  );

  return ready ? loaded : loading;
}
