import httpClient from './HttpClient';
import { API_KEY } from 'react-native-dotenv';

class AuthController {
  constructor() {
  }

  token = async () => {
    this.basePath = "/authentication/token/new?api_key="+API_KEY;

    return await httpClient.get(this.basePath, {})
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }


  createSession = async (token) => {
    this.basePath = "/authentication/session/new?api_key="+API_KEY;
    
    return await httpClient.post(this.basePath, {
      request_token: token.token
    })
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }
  
}

export default new AuthController();
