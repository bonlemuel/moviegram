import httpClient from './HttpClient';
import { API_KEY } from 'react-native-dotenv';

class MovieController {
  constructor() {
    // this.basePath = "/movie/popular?api_key="+API_KEY+"&language=en-US&page=1";
  }

  getTrendingMovie = async (type) => {
    this.basePath = "/trending/movie/"+type+"?api_key="+API_KEY;

    return await httpClient.get(this.basePath, {})
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  getMovieDetails = async (id) => {
    this.detailsPath = "/movie/"+id+"?api_key="+API_KEY;
    this.creditsPath = "/movie/"+id+"/credits?api_key="+API_KEY;
    this.reviewsPath = "/movie/"+id+"/reviews?api_key="+API_KEY;

    return await Promise.all([
      httpClient.get(this.detailsPath),
      httpClient.get(this.creditsPath),
      httpClient.get(this.reviewsPath)
    ])
    .then(([details, credits, reviews]) => {
      return { data : { details: details.data, credits: credits.data, reviews: reviews.data} }
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  getSearchMovie = async (query) => {
    this.basePath = "/search/movie?api_key="+API_KEY+"&language=en-US&query="+query+"&page=1&include_adult=false";

    return await httpClient.get(this.basePath, {})
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  getWatchlist = async (sessionID) => {
    this.basePath = "https://api.themoviedb.org/3/account/{account_id}/watchlist/movies?api_key="+API_KEY+"&language=en-US&session_id="+sessionID+"&sort_by=created_at.desc&page=1";

    return await httpClient.get(this.basePath, {})
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  getRatedMovies = async (sessionID) => {
    this.basePath = "https://api.themoviedb.org/3/account/{account_id}/rated/movies?api_key="+API_KEY+"&language=en-US&session_id="+sessionID+"&sort_by=created_at.desc&page=1";

    return await httpClient.get(this.basePath, {})
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  addToWatchlist = async (movieID, sessionID) => {
    this.basePath = "/account/{account_id}/watchlist?api_key="+API_KEY+"&session_id="+sessionID;
     
    return await httpClient.post(this.basePath, {
      media_type: "movie",
      media_id: movieID,
      watchlist: true
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  removeFromWatchlist = async (movieID, sessionID) => {
    this.basePath = "/account/{account_id}/watchlist?api_key="+API_KEY+"&session_id="+sessionID;
     
    return await httpClient.post(this.basePath, {
      media_type: "movie",
      media_id: movieID,
      watchlist: false
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  rateMovie = async (rating, movieID, sessionID) => {
    this.basePath = "/movie/"+movieID+"/rating?api_key="+API_KEY+"&session_id="+sessionID

    return await httpClient.post(this.basePath, {
      value: rating
    })
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

  unrateMovie = async (movieID, sessionID) => {
    this.basePath = "/movie/"+movieID+"/rating?api_key="+API_KEY+"&session_id="+sessionID
    
    return await httpClient.delete(this.basePath, {})
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      console.log(error);
      return error;
    });
  }

}

export default new MovieController();
