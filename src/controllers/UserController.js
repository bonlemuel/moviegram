import httpClient from './HttpClient';
import { API_KEY } from 'react-native-dotenv';

class UserController {
  constructor() {
    this.basePath = "/authentication/token/validate_with_login?api_key="+API_KEY;
  }

  login = async (username, password, token) => {
    return await httpClient.post(this.basePath, {
      username: username,
      password: password,
      request_token: token.token
    })
    .then(function (response) {
      console.log(response)
      return response
    })
    .catch(function (error) {
      console.log("error here", error);
      return null;
    });
  }

  logout = () => null;
  
}

export default new UserController();
