import React from 'react';
import { Image } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';

import Settings from '../Settings';
import Home from '../Home';
import Watchlist from '../Watchlist';
import Movie from '../Movie';
import Login from '../Login';

import homeIcon from 'assets/ic_home/ic_home.png';
import faveIcon from 'assets/ic_fave/ic_fave.png';
import settingsIcon from 'assets/ic_settings/ic_settings.png';
import Colors from 'helpers/Colors';

const iconForTab = ({ state }) => {
  switch (state.routeName) {
    case 'Home':
      return homeIcon;
    case 'Watchlist':
      return faveIcon;
    case 'Settings':
      return settingsIcon;
    default:
      return null;
  }
};

const TabIcon = ({ icon, tintColor }) => (// eslint-disable-line
  <Image
    source={icon}
    style={{ tintColor }}
  />
);

const HomeStack = createStackNavigator({ Home, Movie },{ headerLayoutPreset: 'center' });
const WatchlistStack = createStackNavigator({ Watchlist, Movie },{ headerLayoutPreset: 'center' });
const SettingsStack = createStackNavigator({ Settings },{ headerLayoutPreset: 'center' });

const AppStack = createBottomTabNavigator(
  {
    Home: HomeStack,
    Watchlist: WatchlistStack,
    Settings: SettingsStack,
  },
  {
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBarOptions: {
      activeTintColor: Colors.active,
      inactiveTintColor: Colors.inactive,
      style: {
        backgroundColor: Colors.tab,
      },
    },
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ tintColor }) => (// eslint-disable-line
        <TabIcon
          icon={iconForTab(navigation)}
          tintColor={tintColor}
        />
      ),
    }),
  },
);

export default AppStack;
