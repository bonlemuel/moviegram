import React, { useCallback, useEffect } from 'react';
import {
  View,
  Text,
  Dimensions,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { GridView, TextView } from '../common';

import styles from './styles';

import TextStyles from 'helpers/TextStyles';
import Colors from 'helpers/Colors';
import strings from 'localization';
import getUser from 'selectors/UserSelectors';
import getToken from 'selectors/AuthSelectors';
import getMovies from 'selectors/MovieSelectors';

import { getWatchListDispatch } from 'actions/MovieActions';

function Watchlist(props) {
  // states
  const user = useSelector(state => getUser(state));
  const token = useSelector(state => getToken(state));
  const movies = useSelector(state => getMovies(state));

  // actions
  const dispatch = useDispatch();
  const getWatchListFunc = useCallback(() => dispatch(getWatchListDispatch(token.session_id)));

  useEffect(() => {
    getWatchListFunc();
  },[]);

  function refreshGrid(){
    getWatchListFunc();
  }

  return (
    <View style={styles.container}>
      <TextView type={"1"} label={"Added to Watchlist"}/>
      {
        movies.watchlist ?
          movies.watchlist.results.length > 0 ?
            <GridView 
              type={'Watchlist'}
              navigation={props.navigation}
              data={movies.watchlist}
              callback={refreshGrid.bind(this)}/>
            : <Text style={{color: 'white', textAlign: 'center', paddingTop: Dimensions.get('window').height / 4}}>{'No movie in your watchlist'}</Text>
          : <Text style={{color: 'white', textAlign: 'center', paddingTop: Dimensions.get('window').height / 4}}>{'No movie in your watchlist'}</Text>
      }
    </View>
  );
}

Watchlist.navigationOptions = {
  title: strings.saved,
  headerStyle: {
    backgroundColor: Colors.header,
    borderBottomWidth: 0,
  },
  headerTintColor: Colors.headerText,
  headerTitleStyle: {
    fontWeight: 'bold',
    fontSize: 20
  },
};

export default Watchlist;
