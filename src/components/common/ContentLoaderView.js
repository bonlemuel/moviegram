import React, { Component } from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';

import ContentLoader from 'react-native-content-loader';
import {Circle, Rect} from 'react-native-svg';

import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
  contenLoader: {
    flex: 1, 
    alignItems: 'center', 
    paddingTop: 50, 
    backgroundColor: Colors.background, 
    height: '100%', 
    width: '100%'
  }
});

const MovieLoader = () => (
  <View style={styles.contenLoader}>
    <ContentLoader 
      primaryColor={Colors.loaderPrimary}
      secondaryColor={Colors.loaderSecondary}
      height={1000}>
      <Rect x={(Dimensions.get('window').width / 3) - 50} y="10" rx="4" ry="4" width={(Dimensions.get('window').width / 3) - 12} height="200"/>
      <Rect x={(Dimensions.get('window').width / 3) - 60} y="230" rx="4" ry="4" width={140} height={15}/>
      <Rect x={(Dimensions.get('window').width / 3) - 40} y="260" rx="4" ry="4" width={100} height={10}/>

      <Rect x={(Dimensions.get('window').width / 3) - 70} y="280" rx="4" ry="4" width={50} height={10}/>
      <Rect x={(Dimensions.get('window').width / 3) - 15} y="280" rx="4" ry="4" width={50} height={10}/>
      <Rect x={(Dimensions.get('window').width / 3) + 40} y="280" rx="4" ry="4" width={50} height={10}/>

      <Rect x={(Dimensions.get('window').width / 3) - 130} y="350" rx="4" ry="4" width={140} height={15}/>
      <Rect x={(Dimensions.get('window').width / 3) - 130} y="380" rx="4" ry="4" width={(Dimensions.get('window').width / 1.5)} height={50}/>

      <Rect x={(Dimensions.get('window').width / 3) - 130} y="460" rx="4" ry="4" width={140} height={15}/>
      <Rect x={(Dimensions.get('window').width / 3) - 130} y="490" rx="4" ry="4" width={(Dimensions.get('window').width / 1.5)} height={30}/>
      <Rect x={(Dimensions.get('window').width / 3) - 130} y="530" rx="4" ry="4" width={(Dimensions.get('window').width / 2)} height={20}/>

      <Rect x={(Dimensions.get('window').width / 3) - 130} y="610" rx="4" ry="4" width={100} height={15}/>
      <Rect x={(Dimensions.get('window').width / 3) - 130} y="630" rx="4" ry="4" width={140} height={15}/>

    </ContentLoader>
  </View>
)

export default class ContentLoaderView extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.setState({
      isLoading: true
    })
  }

  componentDidMount() {
    this.setState({
      isLoading: true
    })
  }
  
  componentWillUnmount() {
    this.setState({
      isLoading: false
    })
  }
  
  render() {
    const { isLoading } = this.state;
    return(
      isLoading ?
        MovieLoader()
      : null
    );
  }
  
}
