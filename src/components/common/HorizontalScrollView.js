import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  TouchableHighlight,
  Dimensions,
  Text,
} from 'react-native';
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
    mainView: {
        margin: 5,
    },
    item: {
        flex: 1,
        height: 200,
        margin: 5,
        width: (Dimensions.get('window').width / 3) - 12
    },
    itemAvatar: {
        margin: 15,
        flexDirection: 'column',
    },
    creditAvatar: {
        backgroundColor: 'gray',
        width: 80, 
        height: 80, 
        borderRadius: 40,
        margin: 0,
        padding: 0,
    },
    creditName: {
        color: 'white', 
        textAlign: 'center', 
        paddingTop: 10, 
        fontSize: 11, 
        fontWeight: 'bold',
        width: 80
    },
    creditCharacter: {
        color: 'white', 
        textAlign: 'center', 
        fontSize: 10, 
        fontWeight: '100',
        width: 80
    },
    itemImage: {
        width: (Dimensions.get('window').width / 3) - 12, 
        height: 200
    }
});

const HorizontalScrollView = props => (
    <ScrollView 
        horizontal={true} 
        showsHorizontalScrollIndicator={false} 
        contentContainerStyle={{...styles.mainView, height: props.type === "card" ? 210 : 150}}>
        {
            props.data ?
                props.type === "card" ?
                    props.data.results.map( (data, index, array) =>
                        this._renderItemCard(data, index, props)
                    )
                : props.type === "avatar" ?
                    props.data.map( (data, index, array) =>
                        this._renderItemAvatar(data, index, props)
                    )
                : null
            : null
        }
    </ScrollView>
);

_renderItemCard = (data, key, props) => (
    <View 
        key={key} 
        style={[{backgroundColor: 'gray' }, styles.item]}>
        <TouchableHighlight 
            onPress={()=> props.navigation.navigate('Movie', {...data, from: props.from})}>
            <Image
                style={styles.itemImage}
                source={{
                    uri: "https://image.tmdb.org/t/p/w500" + data.poster_path
                }}
            />
        </TouchableHighlight>
    </View>
);

_renderItemAvatar = (data, key, props) => (
    <View key={key}>
        <View style={styles.itemAvatar}>
            {
                data.profile_path ?
                    <Image
                        style={styles.creditAvatar}
                        source={{
                            uri: "https://image.tmdb.org/t/p/w500" + data.profile_path
                        }}
                    />
                :
                    <Image
                        style={styles.creditAvatar}
                        source={require("../../assets/defaultavatar.png")}
                    />
            }
            <Text style={styles.creditName}>{data.name}</Text>
            <Text style={styles.creditCharacter}>{data.character}</Text>
        </View>
    </View>
);

HorizontalScrollView.propTypes = {
  style: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string
};

HorizontalScrollView.defaultProps = {
  style: null,
};

export default HorizontalScrollView;
