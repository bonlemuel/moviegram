import React, { Component } from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Icon from 'react-native-dynamic-vector-icons';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import moment from "moment";

import { HorizontalScrollView, DialogPopUp, ReviewListView } from "../common";

import Colors from 'helpers/Colors';

class ParallaxView extends Component {
  
  constructor(props) {
    super(props);
    this.state =  {
      id: this.props.data.id,
      title: this.props.data.original_title,
      overview: this.props.data.overview,
      poster: this.props.data.poster_path,
      backdrop: this.props.data.backdrop_path,
      release_date: this.props.data.release_date,
      rating: this.props.data.vote_average,
      genre: this.props.additional.details ? this.props.additional.details.genres[0].name : '',
      tagline: this.props.additional.details ? this.props.additional.details.tagline : '',
      cast: this.props.additional.credits ? this.props.additional.credits.cast : [],
      reviews: this.props.additional.reviews ? this.props.additional.reviews.results : [],
      showDialog: false,
      from: this.props.data.from? this.props.data.from : ''
    };
    this.props = props;
  }

  goBack(){
    this.props.navigation.goBack()
  }

  closePopup(data){
    this.setState({showDialog: data})
  }

  returnRating(data){
    this.setState({showDialog: false}, ()=> this.props.rateCallback(data, this.state.id))
  }

  returnUnrate(){
    this.props.unrateCallback(this.state.id)
  }

  returnValue(text){
    this.props.callback(text);
  }

  removeWatchlist(id){
    this.props.removeWatchlistCallback(id);
  }

  _renderDialog(){
    return(
      <DialogPopUp 
        ratingCallback={this.returnRating.bind(this)}
        callback={this.closePopup.bind(this)} visible={this.state.showDialog}/>
    )
  }

  _renderBackground = () => (
    <View key="background">
      <Image 
        source={{
          uri: "https://image.tmdb.org/t/p/original"+this.state.backdrop,
          width: window.width,
          height: PARALLAX_HEADER_HEIGHT
        }}/>
      <View 
        style={{
          position: 'absolute',
          top: 0,
          width: window.width,
          backgroundColor: 'rgba(0,0,0,.5)',
          height: PARALLAX_HEADER_HEIGHT
        }}/>
    </View>
  )

  _renderForeground = () => (
    <View key="parallax-header" style={ styles.parallaxHeader }>
      
      <Icon name="left" type="AntDesign" size={30} color="white" style={ styles.backButton } onPress={()=> this.goBack()} />
      
      <Image 
        style={ styles.avatar } 
        source={{uri: "https://image.tmdb.org/t/p/w500" + this.state.poster}}/>

      <Text style={ styles.title }>
        {this.state.title}
      </Text>

      <Text numberOfLines={1} style={ styles.tagline }>
        {this.state.tagline}
      </Text>

      <View style={{flex: 1, flexDirection: 'row', alignSelf: 'center', padding: 10}}>
        
          <Text style={ styles.caption }>
            {moment(this.state.release_date).format('YYYY')}
          </Text>
        
        
          <Text style={ styles.caption }>
            {this.state.genre}
          </Text>
        
        <View style={{flexDirection: 'row'}}>
          <Icon name="star" type="AntDesign" size={15} color="white" />
          <Text style={ styles.ratingCount }>
            {this.state.rating}
          </Text>
        </View>

      </View>
    </View>
  )

  _renderStickyHeader = () => (
    <View key="sticky-header" style={styles.stickySection}>
      <Text style={styles.stickySectionText}>{this.state.title}</Text>
    </View>
  )
    // added a temporary handler of watchlist and rated
  _renderContent = () => (
    <View style={styles.content}>
      
      <View style={{flexDirection: 'row', justifyContent: 'space-around', padding: 20}}>
        { 
          this.state.from !== "rated" ?
            <TouchableOpacity 
              style={{backgroundColor: Colors.buttonPrimary, ...styles.button, ...styles.buttonShadow}} 
              onPress={()=> 
                this.state.from !== "watchlist" ? 
                  this.returnValue(this.state.id) 
                : this.removeWatchlist(this.state.id)}>
              <Text 
                style={styles.buttonText}>
                {this.state.from !== "watchlist" ? 'Add to Watchlist' : 'Remove Watchlist'}
              </Text>
            </TouchableOpacity>
          : null
        }
        { 
          this.state.from !== "watchlist" ?
            <TouchableOpacity 
              style={{backgroundColor: Colors.buttonPrimary, ...styles.button, ...styles.buttonShadow}} 
              onPress={()=> 
                this.state.from !== "rated" ? 
                  this.setState({showDialog: true}) 
                : this.returnUnrate()}>
              <Text 
                style={styles.buttonText}>
                {this.state.from !== "rated" ? 'Rate' : 'Delete Rating'}
              </Text>
            </TouchableOpacity>
          : null
        }
      </View>
      
      <View style={styles.section}>
        <Text style={styles.label}>
          {'Overview'}
        </Text>
        <Text style={styles.description}>
          {this.state.overview}
        </Text>
      </View>

      <View style={styles.section}>
        <Text style={styles.label}>
          {'Cast'}
        </Text>
        <HorizontalScrollView 
          type={'avatar'}
          navigation={this.props.navigation} 
          data={this.state.cast}/>
      </View>

      {
        this.state.reviews.length > 0 ?
          <View style={styles.section}>
            <Text style={styles.label}>
              {'Reviews'}
            </Text>
            <ReviewListView 
              reviews={this.state.reviews}/>
          </View>
        : null
      }

    </View>
  )

  render() {
    return (
      <View style={{ flex: 1 }}>
        <ParallaxScrollView
          backgroundColor={Colors.black}
          contentBackgroundColor={Colors.background}
          stickyHeaderHeight={ STICKY_HEADER_HEIGHT }
          parallaxHeaderHeight={ PARALLAX_HEADER_HEIGHT }
          backgroundSpeed={5}
          renderBackground={this._renderBackground}
          renderForeground={this._renderForeground}
          renderStickyHeader={this._renderStickyHeader}>
          {this._renderContent()}
          {this._renderDialog()}
          </ParallaxScrollView>
        </View>
      );
  }
}

const window = Dimensions.get('window');

const PARALLAX_HEADER_HEIGHT = 380;
const STICKY_HEADER_HEIGHT = ifIphoneX ? 80 : 50;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
  },
  background: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: window.width,
    height: PARALLAX_HEADER_HEIGHT,
  },
  stickySection: {
    height: STICKY_HEADER_HEIGHT,
    width: 300,
    justifyContent: 'flex-end'
  },
  stickySectionText: {
    color: 'white',
    fontSize: 20,
    margin: 10,
  },
  parallaxHeader: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    paddingTop: 50,
  },
  titleGroup: {
    paddingLeft: 20
  },  
  avatar: {
    height: 200,
    width: ( window.width / 3 ) - 10,
  },
  backButton: {
    position: 'absolute',
    top: 60,
    left: 20,
    width: 50,
    height: 50,
  }, 
  title: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    paddingVertical: 5
  },
  tagline: {
    color: 'white',
    fontSize: 14,
    fontWeight: '300',
  },
  caption: {
    color: 'white',
    fontSize: 15,
    fontWeight: '400',
    paddingLeft: 10,
    paddingRight: 10
  },
  ratingCount: {
    color: 'white',
    fontSize: 15,
    fontWeight: '400',
    paddingLeft: 5,
    paddingRight: 10
  },
  content: {
    height: '100%', 
    backgroundColor: Colors.background
  },
  section: {
    margin: 10,
    paddingTop: 20
  },
  label: {
    color: 'white', 
    fontWeight: 'bold', 
    fontSize: 18
  },
  description: {
    color: 'white', 
    fontWeight: '100', 
    fontSize: 14, 
    lineHeight: 20,
    flexWrap: 'wrap', 
    textAlign: 'justify',
    marginTop: 10
  },
  button: {
    width: Dimensions.get('window').width / 2.5, 
    height: 30, 
    justifyContent: 'center', 
    borderRadius: 50
  },
  buttonText: {
    color: '#fff', 
    alignSelf: 'center', 
    textAlign: 'center', 
    fontSize: 14, 
    fontWeight: 'bold'
  },
  buttonShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7
  }
});

export default ParallaxView;