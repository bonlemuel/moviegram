import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableHighlight } from 'react-native';

import SearchBar from "react-native-dynamic-search-bar";
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
});

export default class Searchbar extends Component {
  constructor(props) {
    super(props);

  }

  returnValue(text){
    this.props.callback(text);
  }

  render() {
    return(
      <SearchBar
        height={50}
        onPressToFocus
        autoFocus={false}
        fontColor="#c6c6c6"
        iconColor="#c6c6c6"
        shadowColor="#282828"
        cancelIconColor="#c6c6c6"
        backgroundColor="#343434"
        placeholder="Search"
        onChangeText={text => this.returnValue(text)}
        onPressCancel={() => this.returnValue('')}
        // onPress={() => alert("onPress")}
      />
    );
  }
  
}
