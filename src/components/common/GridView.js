import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableHighlight, Dimensions, RefreshControl } from 'react-native';

import Grid from 'react-native-grid-component';
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
  item: {
    flex: 1,
    height: 180,
    margin: 5,
  },
  list: {
    flex: 1,
    padding: 5,
    backgroundColor: Colors.background
  },
  sectionHeader: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  cardImage: {
    width: '100%', 
    height: 180
  }
});

export default class GridView extends Component {
  constructor(props) {
    super(props);
    this.props = props;
    this.state ={
      isRefreshing: false
    }
  }

  onRefresh() {
    this.setState({isRefreshing: false}, ()=> this.props.callback())
  }

  _renderItem = data => (
      <View  style={[{ backgroundColor: 'gray' }, styles.item]}>
        <TouchableHighlight onPress={()=> this.props.navigation.navigate('Movie', data)}>
          <Image
            style={styles.cardImage}
            source={{uri: "https://image.tmdb.org/t/p/w500/"+data.poster_path}}
          />
        </TouchableHighlight>
      </View>
  );

  _renderPlaceholder = i => <View style={styles.item} key={i} />;

  render() {
    return(
      <Grid
        style={styles.list}
        renderItem={this._renderItem}
        renderPlaceholder={this._renderPlaceholder}
        data={this.props.data.results}
        numColumns={3}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          this.props.type === "Watchlist" ?
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={()=> this.onRefresh()}
            />
          : null
        }
      />
    )
  }
}
