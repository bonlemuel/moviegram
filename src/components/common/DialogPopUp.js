import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} from 'react-native';
import TextStyles from 'helpers/TextStyles';
import Colors from 'helpers/Colors';

import Dialog, { SlideAnimation, DialogFooter, DialogButton, DialogContent } from 'react-native-popup-dialog';
import { Ratings } from '../common';

const styles = StyleSheet.create({
  main: {
    flex: 1
  },
  container: {
    borderRadius: 50
  },
  textMain: {
    paddingTop: 20
  },  
  textLabel: {
    textAlign: 'center', 
    fontSize: 16, 
    fontWeight: 'bold'
  },
  buttonContainer: {
    padding: 10,
  },
  button: {
    width: Dimensions.get('window').width / 2.5, 
    height: 30, 
    justifyContent: 'center', 
    borderRadius: 50,
    alignSelf: 'center'
  },
  buttonText: {
    color: '#fff', 
    alignSelf: 'center', 
    textAlign: 'center', 
    fontSize: 14, 
    fontWeight: 'bold'
  },
  buttonShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,
    elevation: 7
  }
});

const slideAnim = new SlideAnimation({
  slideFrom: 'bottom',
})
let count = 5;


function returnValue(props, data){
  props.callback(data);
}

function returnRating(props, value){
  count = value;
}

const DialogPopUp = props => (
    <View style={styles.main}>
        <Dialog 
          visible={props.visible}
          onTouchOutside={() => {
              returnValue(props, false);
          }}
          dialogAnimation={slideAnim}
          >
          <DialogContent style={styles.container}>
              <View style={styles.textMain}>
                <Text style={styles.textLabel}>{'Rate this movie'}</Text>
              </View>
              <Ratings callback={returnRating.bind(props, this)}/>
              <View style={styles.buttonContainer}>
                <TouchableOpacity style={{backgroundColor: Colors.buttonPrimary, ...styles.button, ...styles.buttonShadow}} onPress={()=> props.ratingCallback(count)}>
                  <Text style={styles.buttonText}>{'Submit'}</Text>
                </TouchableOpacity>
              </View>
          </DialogContent>
        </Dialog>
    </View>
);

DialogPopUp.propTypes = {
  style: PropTypes.object,
};

DialogPopUp.defaultProps = {
  style: null,
};

export default DialogPopUp;
