import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableHighlight } from 'react-native';

import ViewMoreText from 'react-native-view-more-text';

import Colors from 'helpers/Colors';

const styles = StyleSheet.create({

});

export default class ReviewListView extends Component {
  constructor(props) {
    super(props);

  }

  renderViewMore(onPress){
    return(
      <Text onPress={onPress} style={{textAlign: 'right', color: '#fff', borderTopWidth: 1}}>View more</Text>
    )
  }

  renderViewLess(onPress){
    return(
      <Text onPress={onPress} style={{textAlign: 'right', color: '#fff'}}>View less</Text>
    )
  }

  renderReviewContainer(data, index){
      return(
        <View key={index} style={{flex: 1, flexDirection: 'column', paddingTop: 10}}>
            <Text style={{color: '#fff', fontSize: 14, fontWeight: 'bold'}}>
                {data.author}
            </Text>
            <ViewMoreText
                numberOfLines={2}
                renderViewMore={this.renderViewMore}
                renderViewLess={this.renderViewLess}
                textStyle={{textAlign: 'justify', color: '#fff'}}
            >
                <Text style={{color: '#fff', fontSize: 12, fontWeight: '100', lineHeight: 20}}>
                    {data.content}
                </Text>
            </ViewMoreText>
      </View>
      )
  }

  render(){
    return(
        this.props.reviews.map( (data, index) =>
            this.renderReviewContainer(data, index)
        )
    )
  }
  
}

