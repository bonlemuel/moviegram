import React from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  StyleSheet
} from 'react-native';
import TextStyles from 'helpers/TextStyles';
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
    type1: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#fff',
        paddingTop: 30,
        paddingRight: 10,
        paddingLeft: 10,
        paddingBottom: 10
    },
    type2: {

    }
});

const TextView = props => (
  <Text style={props.type === "1" ? styles.type1 : styles.type2}>{props.label}</Text>
);

TextView.propTypes = {
  style: PropTypes.object,
  label: PropTypes.string,
  type: PropTypes.string
};

TextView.defaultProps = {
  style: null,
};

export default TextView;
