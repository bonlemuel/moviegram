import React, { Component } from 'react';
import { StyleSheet, View, Image, Text, TouchableHighlight } from 'react-native';

import { Rating, AirbnbRating } from 'react-native-ratings';
import Colors from 'helpers/Colors';

const styles = StyleSheet.create({
});

export default class Ratings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 5
    }
  }

  returnValue(value){
    this.props.callback(value);
  }

  render() {
    return(
        <AirbnbRating
            defaultRating={5}
            count={10}
            reviews={["Terrible", "Bad", "Meh", "OK", "Average", "Very Good", "Satisfying", "Wow", "Amazing", "Unbelivable!"]}
            size={20}
            onFinishRating={(value)=> this.returnValue(value)}
        />
    );
  }
  
}
