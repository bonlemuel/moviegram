import React, { useCallback, useEffect, useState } from 'react';
import {
  View,
  Text,
  Image,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';

import { Button, ErrorView, TextField } from '../common';
import styles from './styles';

import ShadowStyles from 'helpers/ShadowStyles';
import TextStyles from 'helpers/TextStyles';
import getUser from 'selectors/UserSelectors';
import getToken from 'selectors/AuthSelectors';
import errorsSelector from 'selectors/ErrorSelectors';
import { isLoadingSelector } from 'selectors/StatusSelectors';
import strings from 'localization';
import { login, actionTypes } from 'actions/UserActions';
import { initToken, requestSession } from 'actions/AuthActions';

function Login(props) {

  // states
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  // selectors
  const user = useSelector(state => getUser(state));
  const token = useSelector(state => getToken(state));
  const isLoading = useSelector(state => isLoadingSelector([actionTypes.LOGIN], state));
  const errors = useSelector(state => errorsSelector([actionTypes.LOGIN], state));

  // actions
  const dispatch = useDispatch();
  const tokenRequest = useCallback(() => dispatch(initToken()));
  const loginUser = useCallback((token_value) => dispatch(login(username, password, token_value)), [username, password, dispatch]);
  const requestSessionFunc = useCallback((token_value) => dispatch(requestSession(token_value)), [dispatch]);
  const usernameChanged = useCallback(value => setUsername(value), []);
  const passwordChanged = useCallback(value => setPassword(value), []);

  useEffect(() => {
    
    if (user !== null) {
      requestSessionFunc(token);
      props.navigation.navigate('App');
    }

    if(token.token === null && token.session_id === null){
      console.log("[ ************ token requested ************ ]");
      tokenRequest();
    }

  },[user]);

  function loginUserFunc(){
    loginUser(token)
  }

  return (
    <View style={styles.container}>
      <View style={[styles.formContainer, ShadowStyles.shadow]}>
        <Image
          resizeMode={'contain'}
          style={styles.imageLogo}
          source={require("../../assets/investaflixlogo.png")}
        />
        <Text style={TextStyles.fieldTitle}>
          {strings.username}
        </Text>
        <TextField
          placeholder={strings.username}
          onChangeText={usernameChanged}
          value={username}
        />
        <Text style={TextStyles.fieldTitle}>
          {strings.password}
        </Text>
        <TextField
          placeholder={strings.password}
          value={password}
          onChangeText={passwordChanged}
          secureTextEntry
        />
        <ErrorView errors={errors} />
        <Button
          onPress={()=> {loginUserFunc()}}
          title={isLoading ? strings.loading : strings.login}
        />
      </View>
    </View>
  );
}

Login.navigationOptions = {
  header: null,
};

Login.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Login;