import React, { useCallback, useEffect } from 'react';
import {
  View,
  Text,
  Dimensions
} from 'react-native';
import PropTypes from 'prop-types';
import { useState, useSelector, useDispatch } from 'react-redux';

import { ContentLoaderView, ParallaxView } from '../common';

import styles from './styles';

import strings from 'localization';
import Colors from 'helpers/Colors';

import TextStyles from 'helpers/TextStyles';
import getSelected from 'selectors/MovieSelectors';
import getToken from 'selectors/AuthSelectors';

import { getMovieDetailsDispatch, addMovieWatchListDispatch, rateMovieDispatch, unrateMovieDispatch, removeMovieWatchListDispatch } from 'actions/MovieActions';


function Movie(props) {

  // selectors
  const movie = useSelector(state => getSelected(state));
  const token = useSelector(state => getToken(state));

  // actions
  const dispatch = useDispatch();
  const getSelectedData = useCallback(() => dispatch(getMovieDetailsDispatch(props.navigation.state.params.id)));
  const addtoWatchlistFunc = useCallback((id) => dispatch(addMovieWatchListDispatch(id, token.session_id)));
  const removeFromWatchlistFunc = useCallback((id) => dispatch(removeMovieWatchListDispatch(id, token.session_id)));
  const rateMovieFunc = useCallback((value, id) => dispatch(rateMovieDispatch(value, id, token.session_id)));
  const unrateMovieFunc = useCallback((id) => dispatch(unrateMovieDispatch(id, token.session_id)));

  useEffect(() => {
    if(movie.selected !== null){
      if(movie.selected.data.details.id !== props.navigation.state.params.id){
        getSelectedData();
      }
    } else {
      getSelectedData();
    }
  });

  function addWatchlist(id){
    addtoWatchlistFunc(id)
  }

  function removeWatchlist(id){
    removeFromWatchlistFunc(id)
  }

  function getRateResponse(value, id){
    rateMovieFunc(value, id)
  }

  function getUnrateResponse(id){
    unrateMovieFunc(id)
  }

  return (
    movie.selected !== null?
      movie.selected.data.details.id === props.navigation.state.params.id ?    
        <ParallaxView
          navigation={props.navigation} 
          data={props.navigation.state.params}
          additional={movie.selected.data}
          callback={addWatchlist.bind(this)}
          removeWatchlistCallback={removeWatchlist.bind(this)}
          rateCallback={getRateResponse.bind(this)}
          unrateCallback={getUnrateResponse.bind(this)}>
        </ParallaxView>
        : <ContentLoaderView />
        : <ContentLoaderView />
  );
}

Movie.navigationOptions = {
  header: null,
};

export default Movie;
