import React, { useCallback, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert
} from 'react-native';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';

import Button from '../common/Button';
import styles from './styles';

import strings from 'localization';
import Colors from 'helpers/Colors';

import TextStyles from 'helpers/TextStyles';
import { logout } from 'actions/UserActions';
import { globalReset } from 'actions/GlobalActions';
import getUser from 'selectors/UserSelectors';


function Settings(props) {

  const options = [{
    name: 'Version',
    sub: '1.0.0',
    function: ''
  },{
    name: 'Build',
    sub: '0.0.1',
    function: ''
  }]

  const user = useSelector(state => getUser(state));
  const dispatch = useDispatch();
  const logoutUser = useCallback(() => dispatch(logout()), [dispatch]);

  useEffect(() => {
    if (user === null) {
      props.navigation.navigate('Auth');
    }
  });

  function showAbout(){
    Alert.alert(
      'About this app',
      'A technical exam for Investagrams, submitted by Bon Lemuel T. Dela Cruz',
      [
        {
          text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }

  function logoutUserAccount(){
    logoutUser();
    globalReset();
  }

  return (
    <View style={styles.container}>
      {
        options.map( (data, index, array) =>
          <View key={index} style={{flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: '#fff', borderBottomWidth: 1}}>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold', paddingTop: 10, paddingBottom: 10}}>{data.name}</Text>
            <Text style={{color: '#fff', fontSize: 16, fontWeight: '100', paddingTop: 10, paddingBottom: 10}}>{data.sub}</Text>
          </View>

        )
      }
       <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: '#fff', borderBottomWidth: 1}} onPress={showAbout}>
        <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold', paddingTop: 10, paddingBottom: 10}}>{'About this App'}</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{flexDirection: 'row', justifyContent: 'space-between', borderBottomColor: '#fff', borderBottomWidth: 1}} onPress={logoutUserAccount}>
        <Text style={{color: '#fff', fontSize: 16, fontWeight: 'bold', paddingTop: 10, paddingBottom: 10}}>{'Logout'}</Text>
      </TouchableOpacity>
    </View>
  );
}

Settings.navigationOptions = {
  title: strings.settings,
  headerStyle: {
    backgroundColor: Colors.header,
    borderBottomWidth: 0,
  },
  headerTintColor: Colors.headerText,
  headerTitleStyle: {
    fontWeight: 'bold',
    fontSize: 20
  },
};

Settings.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Settings;
