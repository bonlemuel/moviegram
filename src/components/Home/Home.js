import React, { useState, useCallback, useEffect } from 'react';
import {
  ScrollView,
  RefreshControl,
  View,
  Text,
  Dimensions
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { GridView, HorizontalScrollView, Searchbar, TextView } from '../common';

import styles from './styles';

import TextStyles from 'helpers/TextStyles';
import Colors from 'helpers/Colors';
import strings from 'localization';
import getAllStates from 'selectors/GlobalSelectos';
import getMovies from 'selectors/MovieSelectors';
import getToken from 'selectors/AuthSelectors';

import { getTodayTrendingMovies, getWeeklyTrendingMovies, getSearchMovieDispatch, getWatchListDispatch, getRatedMovieDispatch } from 'actions/MovieActions';

function Home(props) {
  // state
  const [searchKey, setQuery] = useState('');
  const [isRefreshing, setRefresh] = useState(false);

  // selectors
  const global = useSelector(state => getAllStates(state));
  const movies = useSelector(state => getMovies(state));
  const token = useSelector(state => getToken(state));

  // actions
  const dispatch = useDispatch();
  const getTodayTrending = useCallback(() => dispatch(getTodayTrendingMovies()));
  const getWeeklyTrending = useCallback(() => dispatch(getWeeklyTrendingMovies()));
  const searchMovie = useCallback((key) => dispatch(getSearchMovieDispatch(key)));
  const getWatchListFunc = useCallback(() => dispatch(getWatchListDispatch(token.session_id)));
  const getRatedMoviesFunc = useCallback(() => dispatch(getRatedMovieDispatch(token.session_id)));

  const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 20;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };

  useEffect(() => {
    if(movies.today === null){
      getTodayTrending();
    }
    if(movies.weekly === null){
      getWeeklyTrending();
    }
    if(token.session_id){
      getWatchListFunc();
    }
    if(token.session_id){
      getRatedMoviesFunc();
    }
    
    if(movies.search !== null){

    }
  }, [token]);

  function getResponse(result){
    setQuery(result);
    searchMovie(result);
  }

  function onRefresh() {
    setRefresh(true);
    getTodayTrending();
    getWeeklyTrending();
    getWatchListFunc();
    getRatedMoviesFunc();
    setRefresh(false);
  }

  return (
    <ScrollView 
      style={styles.container} 
      contentContainerStyle={{paddingBottom: 30}}
      refreshControl={
        <RefreshControl
          refreshing={isRefreshing}
          onRefresh={onRefresh}
        />
      }
      >
      <Searchbar callback={getResponse.bind(this)}/>

      {
        searchKey === '' ?
          <View>
            <TextView 
              type={"1"} 
              label={"Today's Trending"}/>
            <HorizontalScrollView 
              type={'card'}
              navigation={props.navigation} 
              data={movies.today}/>

            <TextView 
              type={"1"} 
              label={"Trending this Week"}/>
            <HorizontalScrollView 
              type={'card'}
              navigation={props.navigation} 
              data={movies.weekly}/>
          </View>
        : 
        <View>
          <TextView 
            type={"1"} 
            label={"Results"}/>
            {
              movies.search ?
              movies.search.results.length > 0 ?
                <GridView 
                type={"Home"}
                navigation={props.navigation}
                data={movies.search}/>
                : <Text style={{color: 'white', textAlign: 'center', paddingTop: Dimensions.get('window').height / 4}}>{'No result found'}</Text>
              : null
            }
        </View>
      }

      {
        movies.watchlist && token.session_id && searchKey === '' ?
          movies.watchlist.results.length > 0  ?  
            <View>
              <TextView 
                type={"1"} 
                label={"Recently Watchlisted"}/>
              <HorizontalScrollView 
                type={'card'}
                from={'watchlist'}
                navigation={props.navigation} 
                data={movies.watchlist !== null ? movies.watchlist : []}/>
            </View>
          : null
        : null
      }

      {
        movies.rated && token.session_id && searchKey === '' ?
          movies.rated.results.length > 0 ?  
            <View>
              <TextView 
                type={"1"} 
                label={"Recently Rated"}/>
              <HorizontalScrollView 
                type={'card'}
                from={"rated"}
                navigation={props.navigation} 
                data={movies.rated}/>
            </View>
          : null
        : null
      }

    </ScrollView>
  );
}

Home.navigationOptions = {
  title: strings.home,
  headerStyle: {
    backgroundColor: Colors.header,
    borderBottomWidth: 0,
  },
  headerTintColor: Colors.headerTextHome,
  headerTitleStyle: {
    fontWeight: 'bold',
    fontSize: 20,
  },
};

export default Home;
