export default {
  primary: '#1976D2',
  red: '#e53935',
  white: '#FAFAFA',
  gray: '#CFD8DC',
  black: '#000000',
  header: '#141414',
  headerText: '#ffffff',
  headerTextHome: '#E50914',
  tab: '#000000',
  active: '#ffffff',
  inactive: '#564d4d',
  background: '#141414',
  secondaryBackground: '#424242',
  loaderPrimary: '#2b2b2b',
  loaderSecondary: '#424242',
  buttonPrimary: '#E50914',
  buttonRate: '#4c4c4c'
};
